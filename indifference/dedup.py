from models import File, FileData, PerceptualHash
from pathlib import Path
from hexhamming import hamming_distance_string
from tqdm import tqdm
import utils
from config import config


def exact(path, dry_run):
    norm_path = utils.normalize_path(path)
    matching_file = File.get(File.path == norm_path)
    sha1sum = matching_file.data.sha1sum
    matches = utils.files_for_sha1sum(sha1sum)
    if len(matches) > 1:
        print(f"Duplicates for {sha1sum}:")
        utils.print_match_list(matches)
        if not dry_run:
            path.unlink()
            matching_file.delete()


def perceptual(path, dry_run):
    norm_path = utils.normalize_path(path)
    matching_file = File.get(File.path == norm_path)
    phash = matching_file.data.phash.value
    type = matching_file.data.phash.type
    matches = utils.files_for_phash(type, phash)

    if len(matches) > 1:
        print(f"Duplicates for {phash}:")
        utils.print_match_list(matches)
        if not dry_run:
            handle_perceptual_duplicates(matching_file, matches)


def compare_set(all_references, new_files, mode):
    duplicate_files = {}
    for reference_phash in tqdm(all_references, desc=f"Comparing {mode} phashes"):
        for new_file in new_files:
            dist = hamming_distance_string(new_file[0], reference_phash.value)
            lengths_match = new_file[1].data.phash.length == reference_phash.length
            if dist < int(config['indifference'][f'{mode}_threshold']) and lengths_match:
                found_duplicates = [x for forms in reference_phash.forms for x in forms.files if x.path != new_file[1].path]
                # Take action IF there are still duplicates
                if len(found_duplicates) > 0:
                    if new_file in duplicate_files:
                        duplicate_files[new_file].extend(found_duplicates)
                    else:
                        duplicate_files[new_file] = found_duplicates
    return duplicate_files


def perceptual_batch(dedup_files, dry_run):
    new_videos = []
    new_images = []
    for new_file in tqdm(list(dedup_files), desc="Gathering new files"):
        try:
            new_file_path = utils.normalize_path(new_file)
            matching_file = File.get(File.path == new_file_path)
            phash = matching_file.data.phash.value
            type = matching_file.data.phash.type
            if type == "image":
                new_images.append((phash, matching_file))
            if type == "video":
                new_videos.append((phash, matching_file))
        except AttributeError:
            pass

    all_image_phashes = PerceptualHash.select().where(PerceptualHash.type == "image")
    duplicate_images = compare_set(all_image_phashes, new_images, "image")

    all_video_phashes = PerceptualHash.select().where(PerceptualHash.type == "video")
    duplicate_videos = compare_set(all_video_phashes, new_videos, "video")

    for file, matches in duplicate_images.items():
        handle_perceptual_duplicates(file[1], matches)
    for file, matches in duplicate_videos.items():
        handle_perceptual_duplicates(file[1], matches)


def handle_perceptual_duplicates(this_file, matches):
    utils.clear_working_dir()
    print("")
    print("New file:")
    print(f"  {this_file.path} ({this_file.id}): {this_file.data.phash.value}")
    print("Potential duplicates:")
    existing_dupes = False
    auto_delete = False
    for match in matches:
        hd = hamming_distance_string(this_file.data.phash.value, match.data.phash.value)
        if match.abs_path().exists():
            stat = ""
            existing_dupes = True
            if hd <= 6:
                auto_delete = True
        else:
            stat = "(removed)"
        print(f"  {match.id} {match.path}: {match.data.phash.value} delta {hd} {stat}")
    print("")

    if not existing_dupes:
        print("(Skipping, no duplicates still exist to compare)")
        return

    if auto_delete:
        print("Close match, deleting new file automatically.")
        selections = [this_file.id, ]
    else:
        utils.create_working_dir(matches + [this_file,])
        print("All matches have been placed in indifferences/. You can choose to delete one or more matches, or")
        print("press enter to retain existing files and discard the new one, or 'x' to keep all.")
        print(f"The newly added file is {this_file.id}")
        while True:
            selection = input(f"Enter IDs to delete, space separated, or x [{this_file.id}]: ")
            try:
                if selection == 'x':
                    selections = []
                elif selection:
                    selections = selection.split(" ")
                    selections = [int(x) for x in selections]
                else:
                    selections = [this_file.id, ]

                break

            except Exception as e:
                print(f"There was a problem with your selection: {e}")

    for id in selections:
        obj = File.get(File.id == id)
        obj.abs_path().unlink()
        print(f"Deleted {id}: {obj.abs_path()}")
    utils.clear_working_dir()
