from peewee import *
from config import config
from pathlib import Path

database = SqliteDatabase(None)


class BaseModel(Model):
    class Meta:
        database = database


class PerceptualHash(BaseModel):
    id = AutoField(primary_key=True)
    value = TextField(index=True)
    length = IntegerField(null=True)  # in seconds, for video only
    type = TextField()  # 'image' or 'video' based on MIME type (or 'unknown')


class FileData(BaseModel):
    id = AutoField(primary_key=True)
    sha1sum = TextField(null=True, index=True)
    phash = ForeignKeyField(PerceptualHash, backref='forms', index=True, null=True, default=None)


class File(BaseModel):
    id = AutoField(primary_key=True)
    path = TextField(index=True, unique=True)
    data = ForeignKeyField(FileData, backref='files', index=True)

    def abs_path(self):
        norm_path = Path(self.path)
        base_path = Path(config['paths']['base_path'])
        return base_path / norm_path
