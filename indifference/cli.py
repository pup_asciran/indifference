import click
import models
from pathlib import Path
import os
from tqdm import tqdm
import loader
import dedup
import utils
from config import config
from models import PerceptualHash, FileData


@click.group()
@click.pass_context
def cli(ctx):
    """Specialized deduplication tool for LEWDIX"""
    models.database.init(Path(config['indifference']['db_path']))


@cli.command()
@click.option("--dry-run", "-d", is_flag=True, help="Do not delete duplicates")
@click.argument("path", required=True)
def load(path, dry_run):
    new_file_iter = tqdm(list(utils.scan_dir(path)), unit="file", smoothing=0)
    for new_file in new_file_iter:
        new_file_iter.set_description(f"sha1 {new_file}")
        loader.process_file(new_file)
        dedup.exact(new_file, dry_run)

    new_file_iter = tqdm(list(utils.scan_dir(path)), unit="file", smoothing=0)
    for new_file in new_file_iter:
        new_file_iter.set_description(f"phash {new_file}")
        loader.analyze_file(new_file)
    # In this case we deduplicate as a second step, so that the user interaction all happens at the end
    dedup.perceptual_batch(utils.scan_dir(path), dry_run)


@cli.command()
def init_db():
    """Initialize empty/nonexistent SQL database"""
    models.database.create_tables([models.File, models.FileData, models.PerceptualHash])
    print(f"Initialized database at {config['indifference']['db_path']}")


@cli.command()
@click.argument("hash", default=None, required=True)
@click.option("--copy-to", "-c", is_flag=True)
def identical(hash, copy_to):
    """List files matching given file hash"""
    matches = utils.files_for_sha1sum(hash)
    utils.print_match_list(matches)
    if copy_to:
        utils.create_working_dir(matches)


@cli.command()
@click.argument("type", default=None, required=True)
@click.argument("hash", default=None, required=True)
@click.option("--copy-to", "-c", is_flag=True)
def similar(type, hash, copy_to):
    """List files matching given perceptual hash"""
    matches = utils.files_for_phash(type, hash)
    utils.print_match_list(matches)
    if copy_to:
        utils.create_working_dir(matches)


@cli.command()
def clear_video():
    """Temporary tool to remove old-style video hashes"""
    bad_hashes = PerceptualHash.select().where(PerceptualHash.type == "video")
    print(f"Clearing {bad_hashes.count()} video hashes.")
    for bad_hash in bad_hashes:
        related_datas = bad_hash.forms
        for data in related_datas:
            data.phash = None
            data.save()
        bad_hash.delete_instance()
    print("Finished removing video phashes")


if __name__ == "__main__":
    cli()
