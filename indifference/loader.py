import mimetypes
from models import File, FileData, PerceptualHash
from pathlib import Path
import utils
from config import config
import hashlib
from PIL import Image
import imagehash
import cv2


def file_hash(path):
    sha1 = hashlib.sha1()
    with open(path, 'rb') as f:
        while True:
            data = f.read(65536)
            if not data:
                break
            sha1.update(data)
    return sha1.hexdigest()


def process_file(path):
    norm_path = utils.normalize_path(path)
    matching_file = File.get_or_none(File.path == norm_path)

    if not matching_file:
        sha1sum = file_hash(path)
        matching_data = FileData.get_or_none(FileData.sha1sum == sha1sum)
        if not matching_data:
            matching_data = FileData()
            matching_data.sha1sum = sha1sum
            matching_data.save()

        file = File()
        file.path = norm_path
        file.data = matching_data
        file.save()


def video_hash(path):
    temp_base = Path(config['paths']['temp_path'])
    video = cv2.VideoCapture(path)
    duration = video.get(cv2.CAP_PROP_FRAME_COUNT)
    factor = duration / 5

    cumulative_hash = "0000000000000000"
    for i in range(2, 5):
        offset = factor * i
        video.set(cv2.CAP_PROP_POS_FRAMES, offset)
        success, image = video.read()
        converted = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        pil_image = Image.fromarray(converted)
        frame_hash = str(imagehash.phash(pil_image))
        cumulative_hash = hex(int(cumulative_hash, 16) ^ int(frame_hash, 16))[2:]

    return cumulative_hash


def media_hash(path):
    mime_type = mimetypes.guess_type(path)[0]
    if mime_type:
        mime_type = mime_type.split("/")[0]
    else:
        mime_type = "unknown"

    if mime_type == "video":
        value = video_hash(str(path))
        length = None
    elif mime_type == "image":
        img = Image.open(path)
        ih = imagehash.phash(img)
        value = f"{str(ih):0>16}"
        length = None
    else:
        value = "NA"
        length = None

    return mime_type, value, length


def analyze_file(path):
    norm_path = utils.normalize_path(path)
    matching_file = File.get_or_none(File.path == norm_path)
    matching_data = matching_file.data

    if not matching_data.phash:
        try:
            mime_type, value, length = media_hash(path)
        except cv2.error:
            print(f"Warning: exception when processing video file {path}")
            return

        matching_phash = PerceptualHash.get_or_none(PerceptualHash.type == mime_type, PerceptualHash.value == value,
                                                    PerceptualHash.length == length)
        if not matching_phash:
            matching_phash = PerceptualHash()
            matching_phash.value = value
            matching_phash.type = mime_type
            matching_phash.length = length
            matching_phash.save()

        matching_data.phash = matching_phash
        matching_data.save()
