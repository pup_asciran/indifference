from peewee import fn
import peewee
from pathlib import Path
import os
import shutil
from models import File, FileData, PerceptualHash
from config import config


def scan_dir(path):
    def handle_dir(path):
        for p in path.iterdir():
            if p.is_dir():
                yield from handle_dir(p)
            elif p.is_file():
                yield p

    yield from handle_dir(Path(path))


def normalize_path(path):
    p = Path(path).resolve()
    base = Path(config['paths']['base_path'])
    return p.relative_to(base)


def files_for_phash(type, value):
    matching_phash = PerceptualHash.get_or_none(PerceptualHash.type == type and PerceptualHash.value == value)
    if not matching_phash:
        return []
    result = []
    for form in matching_phash.forms:
        result.extend(form.files)
    return result


def files_for_sha1sum(value):
    matching_data = FileData.get_or_none(FileData.sha1sum == value)
    if not matching_data:
        return []
    else:
        return matching_data.files


def print_match_list(file_list):
    for file in file_list:
        if file.data.phash:
            phash = file.data.phash.value
            type = file.data.phash.type
        else:
            phash = "NULL"
            type = "NULL"

        exists = "" if file.abs_path().exists() else " (removed)"

        print(f"{file.id}{exists}: {file.path} {type} sha1 {file.data.sha1sum} phash {phash}")


def create_working_dir(file_list):
    working_dir = Path.cwd() / "indifferences"
    working_dir.mkdir(exist_ok=True)
    for file in file_list:
        file_path = file.abs_path()
        if file_path.exists():
            shutil.copy(file_path, working_dir / f"{file.id}{file_path.suffix}")


def clear_working_dir():
    working_dir = Path.cwd() / "indifferences"
    working_dir.mkdir(exist_ok=True)
    for file in working_dir.iterdir():
        file.unlink()
